# Billing Module Points

## How to identify user is Paid-NSM user

We can identify user's account status based on following points :

1. `OU` : 
    - ldap entry
    - Command : `sudo ldapsearch -x | grep ou='{ou}' |  grep uid= | cut -d ',' -f 1 | cut -d '=' -f 2 | uniq`
    - value : `nsmapp` (all small case letters)
    - Why not depend on it?
        - Some users are internal so they have ou as 'IITKGP' but they are also Paid-NSM users so in such scenarios we are unable to identify such users as Paid-NSM users.

2. `funded` : 
    - ldap entry
    - value : `Paid-Nsmapp`
    - Why not depend on it?
        - Entry may be present
        - Wrong entry such as spelling error, or case sensetive

3. `Account` :
    - Command : `sshare -a --format=User%40,Account --account=nsmapp`
    - Why not depend on it? 
        - Users listed here might be nsmapp users or may be not such as some CDAC users are also listed here.
        - For Param Shivay the value is `nsmapplications` not `nsmapp`
4. `QOS` :
    - Command : `sudo sacctmgr show assoc format=User%40,qos%40 --noheader -p | grep -e "[a-z]*_nsm|$" | cut -d '|' -f1`
    - Why not depend on it? 
        - We can depend on it but still humman error and may be policy change in future can affect this.

So we have to identify user based on all the three parameters.


## Challenges and their solution

### Opening and Closing balance

**Challenge :** In user bill for Paid Users we mentioning Opening and Closing balance(CPU,GPU Hours) of users, slurm doesn't provide such details so we had to depend on python script to calculate it. 

**Solution :**
- Previously we used to calculate such details using script only and doing few mathematical calculations but later on we come to conclusion that we will store such data in some database/table and will fetch it from there.
- We have created 2 tables in `slurm_acct_db` database which will maintain credited balance of user in `user_credited_balance.py` table and opening balance for each month in `user_opening_balance` table.

### Recharge Scenario

**Challenge :** Slurm only stores current allocated amount (CPU,GPU Hours). If user has consumed his all amount and require to recharge again then slurm don't provide any facilty to store previouly allocated amount. So we were unable to keep track of recharge details of users

**Solution :**
- We created table `user_credited_balance` in `slurm_acct_db` database where we keep all credited amount details of users along with datetime.
- From this credited balance data opening balance also get calculted for that we have created two script `add_credited_balance.py` and `add_opening_balance.py`
- We just need to run `add_credited_balance.py` script which add credited balance of into database (the data come from slurm) and also it will add opening balances for every from from date of credit to till current month(the month in which script is executed).

**Challenge :** Still if user has done recharge to his account the we have to add recharge details in `user_credited_balance` manually.

**Solution :** 
- Created a script (still under testing) which will be shared to system team. Whoever add new user or recharge already existed users will run that script which will add recharge details entry to database.

### Multiple account of same user

**Challenge :** Some users have mutliple accounts depending on there requirements such as user has free account and also he has Paid-NSM account. In such case we will need to generate two seperate bill for such uses

**Solution :**
- For every user there are account and qos allocated. Depening on this properties (Account-QOS) we generate bill.
- For every Account-QOS pair there will be a seperate bill.
- Example : Lets take a a user named _shivamp_ having below account and QOS details.
    - _shivamp_ : _normal_
    - _nsmapp_ : *shivamp_nsm*
    - So user _shivamp_ there will be two seperate bills generated for each Account-QOS pair

### Multiple QOS mapped to single account

**Challenge :** Some users have multiple QOS mapped to one account. Slurm's `sreport` utiltiy calculate usage based on account and not on Account-QOS pair. So if a users have one Paid-NSM qos and a free qos mapped to same account then his resource utilization will sum of both Paid-NSM and free QOS.

```
Command : `sudo sacctmgr show assoc format=User%40,Account%40,qos%40`

qos seperated by comma(,)
```

**Solution :(Not implemented in billing module)** 
- One Account One QOS policy need to adopt
- Instead of `sreport`  utility we can user `sacct` utility we calculate resource utilization (CPU, GPU hours utilization) based on Account-QOS pair.

> Note: We haven't implemented either solution on all clusters, few clusters follows One Account One QOS policy and few has still mutliple QOS mapped to single account. 

### User associated to a QOS for some time duration

**Challenge :**  A user is associated/mapped to a QOS for some time duration and then he is de-associated from that QOS.

Example:
- PARAM Shakti :
    - User : vishwanathph, Account : vishwanathph
    - This user has associated with account `vishwanathph` and has qos `vishwanathph` for some time duration
    - Then he was associated with qos `iitkgp_freeq`

### User associated to a account for some time duration

**Challenge :**  A user is associated/mapped to a account for some time duration and then he is de-associated from that account.

- `sacctmgr show associatins format=account%30,user%30,used user=<userid>` will show only current associaton and not previous/historical association.
- Example : 
    - PARAM Shakti :
        - User : hksahu.iitkgp
        - His current association shows he is mapped to account `hksahu.iitkgp`, but he was also mapped to `iitkgp_free_que` account for some duration.