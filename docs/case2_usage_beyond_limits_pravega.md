# Cluster : PARAM Pravega

## User Details

```
Username :  extktapan
Account : extktapan, iisc
QOS
    - extktapan_nsm (account=extktapan)
    - normal (account=iisc)
```

User Creation Date(Slurm) : 29 Dec 2021 (2021-12-29)<br>
OQS Creation Date(extktapan_nsm) : 16 March 2022 (2022-03-16)<br>

## User's CPU utilization for period : `2021-12-19 to 2022-12-20`

```
$ sudo sreport cluster accountutilizationbyuser user=extktapan start=121921 end=122022 -t min
--------------------------------------------------------------------------------
Cluster/Account/User Utilization 2021-12-19T00:00:00 - 2022-12-19T23:59:59 (31622400 secs)
Usage reported in CPU Minutes
--------------------------------------------------------------------------------
  Cluster         Account     Login     Proper Name        Used       Energy 
--------- --------------- --------- --------------- ----------- ------------ 
parampra+       extktapan extktapan       extktapan   279490214   4280929470 
parampra+            iisc extktapan       extktapan    31632709    647872024
```

For `normal` QOS there is no limit specified, so there is abnormality in above usage.

But for `extktapan_nsm` QOS we had set limit of `cpu=158826600` in GrpTresMins
```
$  sudo sacctmgr show qos qos=extktapan_nsm format=name%40,GrpTresMins%40,flags
                                    Name                              GrpTRESMins                Flags 
---------------------------------------- ---------------------------------------- -------------------- 
                           extktapan_nsm                            cpu=158826600              NoDecay 
```
Still user is able to use resources beyond the specified limit, Why?

User Accociation :-

```
$ sudo sacctmgr show assoc format=Account%30,User%30,Partition,QOS user=extktapan
                       Account                           User  Partition                  QOS 
------------------------------ ------------------------------ ---------- -------------------- 
                     extktapan                      extktapan                   extktapan_nsm 
                          iisc                      extktapan                          normal
```

User has submitted jobs using below QOS for above CPU utilization

```
$ 
       JobID                        Account                            QOS               Start                 End      State 
------------ ------------------------------ ------------------------------ ------------------- ------------------- ---------- 
48046                                  iisc                         normal 2022-03-23T14:45:12 2022-03-23T14:45:12 CANCELLED+ 
48047                                  iisc                         normal 2022-03-23T14:48:24 2022-03-23T14:48:24 CANCELLED+ 
48702                                  iisc                         normal 2022-03-25T11:08:38 2022-03-25T11:08:38 CANCELLED+ 
48706                                  iisc                         normal 2022-03-25T12:30:50 2022-03-25T12:41:12    TIMEOUT 
48788                                  iisc                         normal 2022-03-25T14:56:32 2022-03-25T14:56:32 CANCELLED+ 
48797                                  iisc                         normal 2022-03-25T15:19:04 2022-03-25T15:25:56  COMPLETED 
49426                                  iisc                         normal 2022-03-28T11:28:34 2022-03-28T11:28:39     FAILED 
49603                                  iisc                         normal 2022-03-28T19:40:40 2022-03-28T19:50:50    TIMEOUT 
50261                                  iisc                         normal 2022-03-30T15:23:54 2022-03-30T15:27:02    TIMEOUT 
50390                                  iisc                         normal 2022-03-31T01:29:11 2022-03-31T01:31:40    TIMEOUT 
50408                                  iisc                         normal 2022-03-31T01:32:48 2022-03-31T01:32:51     FAILED 
50411                                  iisc                         normal 2022-03-31T02:00:14 2022-03-31T02:00:17     FAILED 
50412                                  iisc                         normal 2022-03-31T02:00:44 2022-03-31T02:00:47     FAILED 
50423                                  iisc                         normal 2022-03-31T08:10:43 2022-03-31T08:12:45    TIMEOUT 
50430                                  iisc                         normal 2022-03-31T08:22:28 2022-03-31T08:31:06  COMPLETED 
50445                                  iisc                         normal 2022-03-31T10:07:16 2022-03-31T10:15:41     FAILED 
50463                                  iisc                         normal 2022-03-31T10:33:05 2022-03-31T10:40:42  COMPLETED 
50517                                  iisc                         normal 2022-03-31T12:10:38 2022-03-31T12:10:38 CANCELLED+ 
50519                                  iisc                         normal 2022-03-31T14:04:16 2022-03-31T14:12:50  COMPLETED 
50560                                  iisc                         normal 2022-03-31T15:09:34 2022-03-31T15:18:15  COMPLETED 
50811                                  iisc                         normal 2022-04-01T11:44:18 2022-04-01T11:52:57  COMPLETED 
50819                                  iisc                         normal 2022-04-04T05:18:48 2022-04-05T05:14:42     FAILED 
51668                                  iisc                         normal 2022-04-04T17:41:41 2022-04-04T17:41:41 CANCELLED+ 
51857                                  iisc                         normal 2022-04-08T23:01:36 2022-04-09T06:58:33     FAILED 
51865                                  iisc                         normal 2022-04-05T11:10:46 2022-04-05T11:10:46 CANCELLED+ 
53397                                  iisc                         normal 2022-04-13T03:07:38 2022-04-14T03:04:07  COMPLETED 
54007                                  iisc                         normal 2022-04-11T21:52:13 2022-04-11T21:52:13 CANCELLED+ 
54009                                  iisc                         normal 2022-04-11T21:54:01 2022-04-11T22:00:08 CANCELLED+ 
54706                                  iisc                         normal 2022-04-18T09:46:03 2022-04-18T09:46:49     FAILED 
55909                                  iisc                         normal 2022-04-17T12:56:56 2022-04-17T13:06:56    TIMEOUT 
56345                                  iisc                         normal 2022-04-22T17:33:57 2022-04-23T17:29:12  COMPLETED 
57146                                  iisc                         normal 2022-04-21T05:25:15 2022-04-21T05:26:15 CANCELLED+ 
58319                                  iisc                         normal 2022-04-28T23:29:14 2022-04-29T23:24:05  COMPLETED 
59358                                  iisc                         normal 2022-04-28T17:16:49 2022-04-28T17:26:51    TIMEOUT 
60151                                  iisc                         normal 2022-04-29T18:10:34 2022-04-29T18:10:34 CANCELLED+ 
60152                                  iisc                         normal 2022-04-29T18:11:03 2022-04-29T18:11:03 CANCELLED+ 
60230                                  iisc                         normal 2022-04-30T12:05:57 2022-04-30T12:30:32 CANCELLED+ 
60403                                  iisc                         normal 2022-04-30T12:31:15 2022-04-30T12:31:15 CANCELLED+ 
60433                                  iisc                         normal 2022-04-30T12:27:29 2022-04-30T12:27:29 CANCELLED+ 
60434                                  iisc                         normal 2022-05-01T03:14:08 2022-05-01T03:14:14     FAILED 
60532                                  iisc                         normal 2022-05-03T16:28:51 2022-05-03T16:34:45 CANCELLED+ 
60745                                  iisc                         normal 2022-05-05T01:32:24 2022-05-05T01:42:47    TIMEOUT 
61614                                  iisc                         normal 2022-05-10T05:35:36 2022-05-10T16:34:46 CANCELLED+ 
63337                                  iisc                         normal 2022-05-10T10:24:25 2022-05-10T10:24:25 CANCELLED+ 
63689                                  iisc                         normal 2022-05-11T11:50:32 2022-05-11T12:10:59    TIMEOUT 
63980                                  iisc                         normal 2022-05-13T12:12:12 2022-05-13T12:12:25     FAILED 
64349                                  iisc                         normal 2022-05-14T18:49:04 2022-05-14T18:59:26    TIMEOUT 
65632                                  iisc                         normal 2022-05-19T17:01:43 2022-05-19T17:01:43 CANCELLED+ 
66604                                  iisc                         normal 2022-05-20T15:21:55 2022-05-20T17:29:25 CANCELLED+ 
70289                                  iisc                         normal 2022-06-02T18:57:14 2022-06-02T18:57:14 CANCELLED+ 
70374                                  iisc                         normal 2022-06-04T22:39:25 2022-06-04T22:49:49    TIMEOUT 
72509                                  iisc                         normal 2022-06-10T13:31:37 2022-06-10T13:31:37 CANCELLED+ 
72510                                  iisc                         normal 2022-06-10T13:33:35 2022-06-10T13:33:35 CANCELLED+ 
72561                                  iisc                         normal 2022-06-10T15:23:58 2022-06-10T15:23:58 CANCELLED+ 
72584                                  iisc                         normal 2022-06-10T15:59:57 2022-06-10T15:59:57 CANCELLED+ 
72586                                  iisc                         normal 2022-06-10T16:05:49 2022-06-10T16:05:49 CANCELLED+ 
72609                             extktapan                  extktapan_nsm 2022-06-10T17:02:15 2022-06-10T17:02:15 CANCELLED+ 
72621                             extktapan                  extktapan_nsm 2022-06-10T17:20:35 2022-06-10T17:20:35 CANCELLED+ 
72647                             extktapan                  extktapan_nsm 2022-06-10T20:22:43 2022-06-10T20:22:43 CANCELLED+ 
72650                             extktapan                  extktapan_nsm 2022-06-10T20:23:36 2022-06-10T20:23:36 CANCELLED+ 
72651                             extktapan                  extktapan_nsm 2022-06-12T15:41:12 2022-06-13T15:36:22     FAILED 
72654                             extktapan                  extktapan_nsm 2022-06-14T04:05:17 2022-06-15T04:00:22     FAILED 
73824                             extktapan                  extktapan_nsm 2022-06-17T07:13:41 2022-06-18T07:08:21     FAILED 
73995                             extktapan                  extktapan_nsm 2022-06-19T07:18:05 2022-06-20T07:12:48     FAILED 
75105                             extktapan                  extktapan_nsm 2022-06-20T14:32:09 2022-06-21T14:26:56     FAILED 
75505                             extktapan                  extktapan_nsm 2022-06-24T01:17:26 2022-06-24T06:42:51     FAILED 
76559                             extktapan                  extktapan_nsm 2022-06-25T09:18:26 2022-06-26T09:18:30    TIMEOUT 
77375                             extktapan                  extktapan_nsm 2022-06-27T05:31:55 2022-06-28T05:32:00    TIMEOUT 
78122                             extktapan                  extktapan_nsm 2022-06-28T13:21:02 2022-06-28T13:21:02 CANCELLED+ 
78568                             extktapan                  extktapan_nsm 2022-07-01T19:48:57 2022-07-02T08:17:48    TIMEOUT 
79424                                  iisc                         normal 2022-07-04T06:43:32 2022-07-04T13:05:28 CANCELLED+ 
79628                                  iisc                         normal 2022-07-04T13:12:21 2022-07-04T13:12:21 CANCELLED+ 
79630                                  iisc                         normal 2022-07-04T13:26:54 2022-07-04T23:02:36 CANCELLED+ 
80347                             extktapan                  extktapan_nsm 2022-07-06T17:46:45 2022-07-06T17:46:45 CANCELLED+ 
80348                             extktapan                  extktapan_nsm 2022-07-06T17:50:40 2022-07-06T17:50:40 CANCELLED+ 
80349                             extktapan                  extktapan_nsm 2022-07-15T02:26:03 2022-07-16T02:22:17  COMPLETED 
82878                             extktapan                  extktapan_nsm 2022-07-18T17:45:45 2022-07-18T17:45:45  CANCELLED 
105911                            extktapan                  extktapan_nsm 2022-07-20T23:50:24 2022-07-21T23:50:26    TIMEOUT 
105918                            extktapan                  extktapan_nsm 2022-07-21T23:50:28 2022-07-22T13:23:46 CANCELLED+ 
106802                            extktapan                  extktapan_nsm 2022-07-22T13:32:40 2022-07-23T07:55:08  NODE_FAIL 
107165                            extktapan                  extktapan_nsm 2022-07-24T10:56:57 2022-07-25T06:19:00     FAILED 
107569                            extktapan                  extktapan_nsm 2022-07-29T10:25:20 2022-07-29T12:55:19 CANCELLED+ 
109257                            extktapan                  extktapan_nsm 2022-07-31T14:16:38 2022-08-01T06:06:07 CANCELLED+ 
109664                            extktapan                  extktapan_nsm 2022-08-04T13:00:38 2022-08-04T18:00:52 CANCELLED+ 
110874                            extktapan                  extktapan_nsm 2022-08-05T18:01:49 2022-08-05T19:58:45 CANCELLED+ 
111096                            extktapan                  extktapan_nsm 2022-08-05T20:05:26 2022-08-05T20:05:31     FAILED 
111097                            extktapan                  extktapan_nsm 2022-08-05T20:11:45 2022-08-05T22:10:41 CANCELLED+ 
111248                            extktapan                  extktapan_nsm 2022-08-06T11:32:51 2022-08-06T11:32:51 CANCELLED+ 
111249                            extktapan                  extktapan_nsm 2022-08-06T22:12:57 2022-08-06T22:33:25    TIMEOUT 
115338                            extktapan                  extktapan_nsm 2022-08-23T14:08:04 2022-08-23T14:08:11     FAILED 
115342                            extktapan                  extktapan_nsm 2022-08-22T20:33:55 2022-08-22T20:34:06     FAILED 
115414                            extktapan                  extktapan_nsm 2022-08-23T07:39:45 2022-08-23T07:39:45 CANCELLED+ 
115432                            extktapan                  extktapan_nsm 2022-08-23T12:02:37 2022-08-23T12:22:41    TIMEOUT 
115543                            extktapan                  extktapan_nsm 2022-08-23T13:47:21 2022-08-23T13:48:13 CANCELLED+ 
115545                            extktapan                  extktapan_nsm 2022-08-23T13:50:26 2022-08-23T14:05:24 CANCELLED+ 
115546                            extktapan                  extktapan_nsm 2022-08-23T14:02:38 2022-08-23T14:02:38 CANCELLED+ 
115547                            extktapan                  extktapan_nsm 2022-08-23T14:08:13 2022-08-23T14:23:29    TIMEOUT 
115554                            extktapan                  extktapan_nsm 2022-08-23T15:18:46 2022-08-23T15:34:02    TIMEOUT 
115571                            extktapan                  extktapan_nsm 2022-08-24T02:08:50 2022-08-24T02:24:17    TIMEOUT 
115761                            extktapan                  extktapan_nsm 2022-08-24T13:32:50 2022-08-24T13:48:16    TIMEOUT 
115764                            extktapan                  extktapan_nsm 2022-08-30T18:05:25 2022-08-30T18:25:49    TIMEOUT 
115843                            extktapan                  extktapan_nsm 2022-08-24T16:12:35 2022-08-24T16:27:47    TIMEOUT 
115919                            extktapan                  extktapan_nsm 2022-08-26T01:27:05 2022-08-26T01:42:31    TIMEOUT 
117513                            extktapan                  extktapan_nsm 2022-09-05T14:10:07 2022-09-06T12:34:55 CANCELLED+ 
118422                            extktapan                  extktapan_nsm 2022-09-09T20:46:15 2022-09-09T20:46:21  COMPLETED 
119386                            extktapan                  extktapan_nsm 2022-09-14T18:35:46 2022-09-14T18:35:46 CANCELLED+ 
120339                            extktapan                  extktapan_nsm 2022-09-15T18:01:49 2022-09-16T17:44:05  COMPLETED 
120702                            extktapan                  extktapan_nsm 2022-09-16T14:51:10 2022-09-16T14:51:10 CANCELLED+ 
120705                            extktapan                  extktapan_nsm 2022-09-16T14:51:35 2022-09-16T14:52:29  COMPLETED 
120715                            extktapan                  extktapan_nsm 2022-09-16T15:18:51 2022-09-16T15:37:35  COMPLETED 
120731                            extktapan                  extktapan_nsm 2022-09-16T16:33:42 2022-09-16T16:55:07  COMPLETED 
120796                            extktapan                  extktapan_nsm 2022-09-17T09:05:47 2022-09-17T10:04:51    TIMEOUT 
120817                            extktapan                  extktapan_nsm 2022-09-17T14:33:54 2022-09-17T15:33:06    TIMEOUT 
120848                            extktapan                  extktapan_nsm 2022-09-17T17:53:22 2022-09-17T18:52:25    TIMEOUT 
120859                            extktapan                  extktapan_nsm 2022-09-17T18:53:06 2022-09-17T19:52:30    TIMEOUT 
120860                            extktapan                  extktapan_nsm 2022-09-17T19:52:37 2022-09-17T19:59:45  COMPLETED 
120914                            extktapan                  extktapan_nsm 2022-09-18T08:00:47 2022-09-18T08:00:47 CANCELLED+ 
120915                            extktapan                  extktapan_nsm 2022-09-18T08:02:28 2022-09-18T08:02:53  COMPLETED 
120916                            extktapan                  extktapan_nsm 2022-09-18T08:07:12 2022-09-18T08:09:29  COMPLETED 
120937                            extktapan                  extktapan_nsm 2022-09-18T17:53:35 2022-09-18T18:23:12 CANCELLED+ 
120945                            extktapan                  extktapan_nsm 2022-09-18T17:53:35 2022-09-18T18:26:57 CANCELLED+ 
120947                            extktapan                  extktapan_nsm 2022-09-23T17:02:26 2022-09-23T20:02:28    TIMEOUT 
121041                            extktapan                  extktapan_nsm 2022-09-19T11:35:02 2022-09-19T12:00:03  COMPLETED 
122687                            extktapan                  extktapan_nsm 2022-09-25T17:51:55 2022-09-25T17:51:55 CANCELLED+ 
122688                            extktapan                  extktapan_nsm 2022-09-28T01:57:23 2022-09-29T01:35:02  COMPLETED 
123670                            extktapan                  extktapan_nsm 2022-10-03T11:55:50 2022-10-04T11:36:18     FAILED 
123984                            extktapan                  extktapan_nsm 2022-10-05T20:38:30 2022-10-05T20:39:09     FAILED 
124244                            extktapan                  extktapan_nsm 2022-10-07T19:16:57 2022-10-08T18:57:58  COMPLETED 
124460                            extktapan                  extktapan_nsm 2022-10-09T11:41:17 2022-10-10T11:03:27 CANCELLED+ 
125228                            extktapan                  extktapan_nsm 2022-10-10T14:19:55 2022-10-11T13:57:37  COMPLETED 
125332                            extktapan                  extktapan_nsm 2022-10-11T13:57:39 2022-10-12T13:40:19  COMPLETED 
125627                            extktapan                  extktapan_nsm 2022-10-13T00:28:09 2022-10-13T16:27:20  NODE_FAIL 
126827                            extktapan                  extktapan_nsm 2022-10-19T23:25:11 2022-10-20T22:55:03  COMPLETED 
126828                            extktapan                  extktapan_nsm 2022-10-20T22:55:05 2022-10-21T22:27:16  COMPLETED 
127678                            extktapan                  extktapan_nsm 2022-10-21T22:27:19 2022-10-22T21:59:42  COMPLETED 
128421                            extktapan                  extktapan_nsm 2022-10-25T10:24:36 2022-10-25T10:25:05     FAILED 
128422                            extktapan                  extktapan_nsm 2022-10-25T10:25:06 2022-10-25T10:41:18     FAILED 
128705                            extktapan                  extktapan_nsm 2022-10-29T04:42:52 2022-10-30T04:20:11  COMPLETED 
128706                            extktapan                  extktapan_nsm 2022-10-30T04:20:12 2022-10-31T03:53:35  COMPLETED 
130259                            extktapan                  extktapan_nsm 2022-11-06T05:26:47 2022-11-07T05:09:54  COMPLETED 
130260                            extktapan                  extktapan_nsm 2022-11-07T05:09:55 2022-11-07T10:42:32 CANCELLED+ 
131512                            extktapan                  extktapan_nsm 2022-11-11T05:34:19 2022-11-12T05:11:07  COMPLETED 
131514                            extktapan                  extktapan_nsm 2022-11-12T05:11:07 2022-11-12T11:46:07 CANCELLED+ 
132926                            extktapan                  extktapan_nsm 2022-11-14T16:21:10 2022-11-14T16:21:10 CANCELLED+ 
132927                            extktapan                  extktapan_nsm 2022-11-14T16:21:11 2022-11-14T16:21:11 CANCELLED+ 
133299                            extktapan                  extktapan_nsm 2022-11-23T08:26:20 2022-11-24T07:58:53     FAILED 
133300                            extktapan                  extktapan_nsm 2022-11-24T07:59:13 2022-11-25T07:35:05  COMPLETED 
136150                            extktapan                  extktapan_nsm 2022-12-01T00:52:26 2022-12-02T00:33:17  COMPLETED 
136389                            extktapan                  extktapan_nsm 2022-12-02T00:33:17 2022-12-03T00:03:20  COMPLETED 
137965                            extktapan                  extktapan_nsm 2022-12-10T12:23:53 2022-12-11T12:07:12  COMPLETED 
138545                            extktapan                  extktapan_nsm 2022-12-11T12:07:13 2022-12-12T11:36:28  COMPLETED 
139873                            extktapan                  extktapan_nsm 2022-12-16T20:21:16 2022-12-17T19:57:14  COMPLETED 
140146                            extktapan                  extktapan_nsm 2022-12-18T00:33:46 2022-12-18T15:33:51    TIMEOUT 
140814                            extktapan                  extktapan_nsm 2022-12-15T19:16:32 2022-12-15T19:26:27  COMPLETED 
141369                            extktapan                  extktapan_nsm             Unknown             Unknown    PENDING 
141779                            extktapan                  extktapan_nsm             Unknown             Unknown    PENDING 
141780                            extktapan                  extktapan_nsm             Unknown             Unknown    PENDING 
```


## Why user can use resources even if his QOS limit is reached?

- `NoDecay` flag is set on extktapan still he was able to use resources.
- Reason : Not yet found.