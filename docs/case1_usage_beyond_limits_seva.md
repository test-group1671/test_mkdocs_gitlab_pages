# Cluster : PARAM Seva

## User Details

```
Username :  alugulab.iitbbs
Account : iitb
QOS
    - nsm (2022-07-04 to 2022-07-28)
    - alugulab.iitbbs_fac (2022-07-29 - NOW)
```

User Creation Date(Slurm) : 07 April 2022 (2022-07-04)<br>
OQS Creation Date(alugulab.iitbbs_fac) : 29 July 2022 (2022-07-29)<br>

## User's CPU utilization for period : `2022-07-04 to 2022-07-28`

```
$ sudo sreport cluster accountutilizationbyuser user=alugulab.iitbbs start=040722 end=072922 -t hour
--------------------------------------------------------------------------------
Cluster/Account/User Utilization 2022-04-07T00:00:00 - 2022-07-28T23:59:59 (9763200 secs)
Usage reported in CPU Hours
--------------------------------------------------------------------------------
  Cluster         Account     Login     Proper Name       Used   Energy 
--------- --------------- --------- --------------- ---------- -------- 
paramseva            iitb alugulab+ alugulab.iitbbs     935950        0
```

User has submitted jobs using QOS `nsm` for above CPU utilization
```
$ sudo sacct -X -u alugulab.iitbbs --format=jobid,account,QOS,start,end,state --starttime=040722 --endtime=072922
       JobID    Account        QOS               Start                 End      State 
------------ ---------- ---------- ------------------- ------------------- ---------- 
4386               iitb        nsm 2022-05-04T15:10:57 2022-05-04T16:34:19     FAILED 
4387               iitb        nsm 2022-05-04T15:18:08 2022-05-04T15:18:08     FAILED 
4388               iitb        nsm 2022-05-04T15:18:13 2022-05-04T16:48:16    TIMEOUT 
4431               iitb        nsm 2022-05-04T20:21:06 2022-05-04T20:22:45     FAILED 
4432               iitb        nsm 2022-05-04T20:26:35 2022-05-04T20:28:12  COMPLETED 
4433               iitb        nsm 2022-05-04T20:35:06 2022-05-04T20:47:09  COMPLETED 
.
.
.
.
```

For `nsm` QOS there is no limit specified, so there is no abnormaly in above usage.
 
## User's CPU utilization for period : `2022-07-29 to 2022-11-30`

```
$ sudo sreport cluster accountutilizationbyuser user=alugulab.iitbbs start=072922 end=113022 -t hour
--------------------------------------------------------------------------------
Cluster/Account/User Utilization 2022-07-29T00:00:00 - 2022-11-29T23:59:59 (10713600 secs)
Usage reported in CPU Hours
--------------------------------------------------------------------------------
  Cluster         Account     Login     Proper Name       Used   Energy 
--------- --------------- --------- --------------- ---------- -------- 
paramseva            iitb alugulab+ alugulab.iitbbs     687095        0
```

User has submitted jobs using QOS `alugulab.iitbbs_fac` for above CPU utilization
```
$ sudo sacct -X -u alugulab.iitbbs --format=jobid,account,QOS,start,end,state --starttime=072922 --endtime=113022 | head
       JobID    Account        QOS               Start                 End      State 
------------ ---------- ---------- ------------------- ------------------- ---------- 
17797              iitb        nsm 2022-07-29T12:30:11 2022-07-29T12:30:17     FAILED (Except this)
17805              iitb        nsm 2022-07-28T23:52:30 2022-07-29T15:51:52 CANCELLED+ (Except this)
17974              iitb alugulab.+ 2022-07-29T23:37:40 2022-07-30T07:53:19  COMPLETED 
17975              iitb alugulab.+ 2022-07-29T23:55:56 2022-07-29T23:56:01     FAILED 
17980              iitb alugulab.+ 2022-07-30T00:17:27 2022-07-30T00:17:33     FAILED 
17982              iitb alugulab.+ 2022-07-30T00:30:48 2022-07-30T02:59:26  COMPLETED 
17986              iitb alugulab.+ 2022-07-30T00:50:33 2022-07-30T03:15:00  COMPLETED 
18030              iitb alugulab.+ 2022-07-30T05:49:06 2022-07-30T14:03:47  COMPLETED 
.
.
.
.
```

But for `alugulab.iitbbs_fac` QOS we had set limit of `billing=100000` in GrpTresMins
```
$ sudo sacctmgr show qos qos=alugulab.iitbbs_fac format=name%40,GrpTresMins%40
               Name      GrpTRESMins 
------------------- ---------------- 
alugulab.iitbbs_fac billing=10000000
```
Still user is able to use resources beyond the specified limit, Why? read below explanation.


## Why user can use resources even if his QOS limit is reached?

- QOS - GrpTresMins will decay if `NoDecay` flag is not set on QOS.
- Why it will decay? 
  - `PriorityDecayHalfLife` parameter is set in slurm.conf file. It will decay/reduce usage i.e. GrpTresMins value (cpu/gpu/billing) by half after provided time period and this reduction process will continue forever till usage become zero.
- When it will decay?
  - The accumulated usage is reduced based on parameter `PriorityCalcPeriod` in in slurm.conf. Currently its 5 mins.
  - The reduction is done by multiplying the accumulated usage by a number slightly less than 1. The number is chosen so that the accumulated usage is reduced to 50 % after PriorityDecayHalfLife (given that you don't run anything more in between, of course). With a halflife of 14 days and the default calc period, that number is very close to 1 (0.9998281).
- Why we enabled `PriorityDecayHalfLife` parameter in slurm.conf?
  - For Fairshare usage, priority will be decided based on mutlifactors. (PriorityType=priority/multifactor in slurm.conf)
  - For faiseshare usage we have to turn on fairshare priority in slurm.
  - When fair share priorities are in use, slurm will reduce accumulated GrpTRESMins over time. This means that it is impossible(*) to use GrpTRESMins limits and fairshare priorities at the same time.
  - But it is possible to tell slurm *not* to reduce the accumulated TRESMins __of a QoS__, so you can technically _use GrpTRESMins limits on a qos, and fair share priorites on the accounts and/or users_.
  - But it might be possible that `NoDecay` flag on QOS will impact fairshare Multifactor Priority of all jobs using this QOS.

> Note : see conversation -> https://groups.google.com/g/slurm-users/c/qqqODaS7UhY

## NoDecay flag is available on QOS but not on association/account GrpTresMins

UserQuestionMail : https://lists.schedmd.com/pipermail/slurm-users/2020-November/006402.html
