# Billing Module Documentation

Billing module is python code which generates user's usage report with billing details in pdf format.

## Types of Users in NSM clusters

### Paid NSM Users
* Funding is provided by NSM
* Have higher priority in job submmision

### External Users

Does not belong to institute where cluster is located, that's why we call them as external users.

* __Paid External Users__
    * Funding is provided by any institute or user pays on his own.
    * Have higher priority in job submmision
* __Free External Users__
    * These are free users so they won't pay any amount
    * Less prirority in job submission

### Internal Users ###

Belongs to institute where cluster is located(either student/faculty of that institute)

* __Free Internal Users__
    * These are free users so they won't pay any amount
    * Less prirority in job submission
* __Free Internal but having billing Users__
    * These are free users so they won't pay any amount
    * Less prirority in job submission
    * Though these are free users they having billing amount allocated.

## Billing Rates for Users

| User Type      | CPU Rate (Rs/Hour) | GPU Rate (Rs/Hour) |
| -------------- | -------------------| -------------------|
| Paid NSM       |  1.5               | 100                |
| Paid External  |  0.96              | 42                 |
| Free Exernal   |  0.96              | 42                 |
| Free Internal(Billing)  |  Site Specific     | Site specific      |
| Free Internal  |  NA     | NA      |

## Billing module directory strcture

* Location : `/home/apps/chakshu/billing`

* Directory Structure & Summary : 

    ![bm_directory_struture](img/bm_directory_structure.png)

    * `fonts` : Consists of font files(.ttf)
    * `images` : Consists of image files which are shown in pdf report as logo.
        - cdac-logo.png
        - site-logo.png
        - nsm-logo.png
    * `invoices` : The generated pdf bills are stored in this directory.
    * `add_credited_balance.py` : Python script to add allocated resource amount of users to the mysql database. (Note : resource amount is CPU/GPU hours allocated to users)
    * `add_opening_balance.py` : Python script to add opening balance i.e available resource amount at the start of every month and on the recharege date of users to the mysql database.
    * `custom_user_bill.py` : Python script to generate bill of a user.
    * `requirements.txt` : Required python dependencies list
    * `userbill_custom` : Python script to generate user bills based on their accounts and for given duration.
    * `userbill_lmonth` : Python script to generate user bills based on their accounts and for lastmonth.
    * `utilities.py` : Python script which consist of comman function and variables which are imported by other scripts.

## Bill Generation

### __NSM paid users__ _or_ __External paid users__ _or_ __Internal free users having billing__

__Prerequisites__

- Credited and Opening balance Entry of such users must be present in database.
- To add credited balance entry run `add_credited_balance.py` script.
- Opening balance entry will be automatically added.

__To Generate bill__

- We can generate bills of paid nsm users by executing `custom_user_bill.py` script.

    - To generate last month bill : `./custom_user_bill.py -u <username>`
    - To generate bill for particular duration : `./custom_user_bill.py -u <username> --starttime=<MMDDYY> --endtime=<MMDDYY>`
    - To generate bill from given duration till today : `./custom_user_bill.py -u <username> --startfrom=<MMDDYY>`
    - The user bill will be generated in `invoices` directory.

### __Internal Free Users__ _or_ __External Free Users__

__Prerequisites__

* No prerequisites for these users
* No need to add entry of credited or opening balance in database for these users as they are not allocated with any resource usage amount.

__To Generate bill__

- Generate bills of these users by executing `custom_user_bill.py` script.

    - To generate last month bill : `./custom_user_bill.py -u <username>`
    - To generate bill for particular duration : `./custom_user_bill.py -u <username> --starttime=<MMDDYY> --endtime=<MMDDYY>`
    - To generate bill from given duration till today : `./custom_user_bill.py -u <username> --startfrom=<MMDDYY>`
    - The user bill will be generated in `invoices` directory.